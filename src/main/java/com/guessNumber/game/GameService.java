package main.java.com.guessNumber.game;

import java.io.BufferedReader;
import java.io.IOException;

public class GameService {
    private Game game;
    private BufferedReader reader;

    public GameService(Game game, BufferedReader reader) {
        this.game = game;
        this.reader = reader;
    }

    public void validateStartingConditions(int min, int max, int numberOfTries) {
        if (min < 1 || max > 200 || min >= max){
            System.out.println("Не корректный диапазон значений интервала. [1, 200]");
            restart();
        }
        if (numberOfTries < 1 || numberOfTries > 15){
            System.out.println("Не корректное количество попыток. [1, 15]");
            restart();
        }

        game.setHiddenNumber((int) (Math.random() * max + min));
    }

    public int validateNumberFromConsole(int number, int min, int max) {
        if (number < min || number > max){
            System.out.println("Введеное число вне диапазона. Попрорбуй заново.");
            return getValueFromConsole();
        } else {
            return number;
        }
    }

    public boolean tryGuessNumber(int guessedNumber, int previousGuess, int hiddenNumber){
        if(guessedNumber == hiddenNumber){
            return true;
        }
        int previousDistance = Math.abs((previousGuess - hiddenNumber));
        int currentDistance = Math.abs((guessedNumber - hiddenNumber));
        if (previousDistance < currentDistance){
            System.out.print("Не угадал(а), холоднее… ");
        } else if(previousDistance > currentDistance){
            System.out.print("Не угадал(а), но теплее!!! ");
        } else {
            System.out.println("Ты на том самом растоянии... ");
        }
        return false;
    }

    public int getValueFromConsole(){
        String readValue = null;
        int readValueInt = 0;
        try {
            System.out.println("Введи число или 'exit' для выхода");
            readValue = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(readValue.equalsIgnoreCase("exit")){
            endGame();
        }
        try {
            readValueInt = Integer.parseInt(readValue);
        } catch (NumberFormatException e) {
            System.out.println("Не корректный ввод! Попробуйте заново.");
            readValueInt = getValueFromConsole();
        }
        return readValueInt;
    }

    public void endGame(){
        System.out.println("Введи 'y' если хочешь начать заново или любой другой символ если хочешь выйти.");
        String message = null;
        try {
            message = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(message.equalsIgnoreCase("y")){
            restart();
        } else {
            System.out.println("Пока!");
            System.exit(0);
        }

    }

    public void win(int tries){
        System.out.println("Поздравляю! Ты угадал(а) задуманное число за " + tries + " попыток");
        endGame();
    }

    private void restart(){
        System.out.println("Введи значение начала интервала [1, 200]");
        game.setMin(getValueFromConsole());
        System.out.println("Введи значение конца интервала [1, 200]");
        game.setMax(getValueFromConsole());
        System.out.println("Введи количество попыток [1, 15]");
        game.setNumberOfTries(getValueFromConsole());
        game.start();
    }
}
