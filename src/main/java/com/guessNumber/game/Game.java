package main.java.com.guessNumber.game;

public class Game {
    private int min;
    private int max;
    private int numberOfTries;
    private int hiddenNumber;
    private GameService gameService;

    protected void setMin(int min) {
        this.min = min;
    }

    protected void setMax(int max) {
        this.max = max;
    }

    protected void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    protected void setHiddenNumber(int hiddenNumber) {
        this.hiddenNumber = hiddenNumber;
    }

    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }

    public Game(int min, int max, int numberOfTries) {
        this.min = min;
        this.max = max;
        this.numberOfTries = numberOfTries;
    }

    public Game() {

    }

    public void start(){
        gameService.validateStartingConditions(min, max, numberOfTries);
        System.out.println("Привет, я загадал число от " + min + " до " + max + " вашего диапазона." +
                " Попробуй угадать его за " + numberOfTries + " попыток!");

        int firstTryValue = gameService.validateNumberFromConsole(gameService.getValueFromConsole(), min, max);
        if (firstTryValue == hiddenNumber){
            gameService.win(1);
        } else {
            System.out.println("Не угадал(а). Осталось " + (numberOfTries - 1) + " попыток");
        }

        int previousTryValue = firstTryValue;
        int valueFromConsole;

        for(int tries = 2; tries < numberOfTries + 1; tries++){
            valueFromConsole = gameService.validateNumberFromConsole(gameService.getValueFromConsole(), min, max);
            if(gameService.tryGuessNumber(valueFromConsole, previousTryValue, hiddenNumber)){
                gameService.win(tries);
            } else {
                System.out.println("Осталось " + (numberOfTries - tries) + " попыток");
                previousTryValue = valueFromConsole;
            }
        }
        System.out.println("Ты не угадал(а) число " + hiddenNumber);
        gameService.endGame();
    }
}
