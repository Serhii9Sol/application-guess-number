package test.java.com.guessNumber;

import main.java.com.guessNumber.game.Game;
import main.java.com.guessNumber.game.GameService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {
    private static GameService cut = new GameService(new Game(), new BufferedReader(new InputStreamReader(System.in)));

    private static Arguments[] tryGuessNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, 50, 20, 50),
                Arguments.arguments(false, 30, 20, 50),
                Arguments.arguments(false, 85, 20, 50),
                Arguments.arguments(false, 60, 40, 50),
        };
    }

    @ParameterizedTest
    @MethodSource("tryGuessNumberTestArgs")
    void tryGuessNumberTest(boolean expected, int guessedNumber, int previousGuess, int hiddenNumber) {
        boolean actual = cut.tryGuessNumber(guessedNumber, previousGuess, hiddenNumber);

        assertEquals(expected, actual);
    }
}