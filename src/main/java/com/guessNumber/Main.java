package main.java.com.guessNumber;

import main.java.com.guessNumber.game.Game;
import main.java.com.guessNumber.game.GameService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Game game1 = new Game(1,100,10);

        GameService gameService = new GameService(game1, reader);

        game1.setGameService(gameService);
        game1.start();

    }
}
